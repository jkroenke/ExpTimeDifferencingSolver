#include <cmath>
#include "particle.h"

Particle::Particle(double initial_position)
{
    position=initial_position;
}

double Particle::getPosition()const
{
    return position;
}

double Particle::getLocaleVelocity(ComplexVector& modes)
{
    double sum=0;
    for(int mode=0;mode<modes.getSize();mode++)
    {   
        sum+=modes.getCoefficient(0,mode)*(cos(mode*position)+cos(-mode*position))-modes.getCoefficient(1,mode)*(sin(mode*position)-sin(-mode*position));
    }
    return sum;
}

void Particle::move(ComplexVector& modes,double time)
{
    position+=getLocaleVelocity(modes)*time;
}
