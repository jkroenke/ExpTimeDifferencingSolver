#include "equationproperties.h"

#ifndef KSEQUATIONPROPERTIES_H
#define KSEQUATIONPROPERTIES_H

class KSEquationProperties: public EquationProperties
{
private:
    double basic_wavevector;
public:
    double lambda(int)const override;
    double nonlinear(bool,ComplexVector&,int)const override;
    KSEquationProperties(double);
};

#endif // KSEQUATIONPROPERTIES_H
