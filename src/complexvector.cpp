#include <cmath>
#include <fstream>
#include "complexvector.h"

void ComplexVector::initialize(int size,double realpart,double imagpart)
{
    for(int i=0;i<size;i++)
    {
        coefficients_real.push_back(realpart);
        coefficients_imag.push_back(imagpart);
    }
}

int ComplexVector::getSize()const
{
    return coefficients_real.size();
}

void ComplexVector::setCoefficient(bool im,int mode_index,double value)
{
    if(im)
    {
        coefficients_imag[mode_index]=value;
    }
    else
    {
        coefficients_real[mode_index]=value;
    }
}

double ComplexVector::getCoefficient(bool im,int mode_index) const
{
    if(im)
    {
        if(std::signbit(mode_index))
        {
            return -coefficients_imag[std::abs(mode_index)];
        }
        else
        {
            return coefficients_imag[std::abs(mode_index)];
        }
    }
    else
    {
        return coefficients_real[std::abs(mode_index)];
    }
}

ComplexVector::ComplexVector()
{
}

ComplexVector::ComplexVector(std::string file)
{
    std::ifstream is;
    is.open(file);
    double val;
    while(is>>val)
    {
        coefficients_real.push_back(val);
        is>>val;
        coefficients_imag.push_back(val);
    }
}
