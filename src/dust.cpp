#include <vector>
#include "dust.h"

Dust::Dust(int number,double start_value,double end_value)
{
    equallyDistribute(number,start_value,end_value);
}

void Dust::equallyDistribute(int number,double start_value,double end_value)
{
    double diff=(end_value-start_value)/number;
    for(int n=0;n<number;n++)
    {
        Particle particle=Particle(n*diff);
        particles.push_back(particle);
    }
}

void Dust::move(ComplexVector modes,double time)
{
    for(int n=0;n<particles.size();n++)
    {
        particles[n].move(modes,time);
    }
}

int Dust::getNumber()const
{
    return particles.size();
}

Particle Dust::getParticle(int index)const
{
    return particles[index];
}
