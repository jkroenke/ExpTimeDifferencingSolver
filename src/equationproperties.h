#include "complexvector.h"

#ifndef EQUATIONPROPERTIES_H
#define EQUATIONPROPERTIES_H

class EquationProperties
{
protected:
    bool linearized=false;
public:
    void setLinearized(bool);
    virtual double lambda(int)const = 0;
    virtual double nonlinear(bool,ComplexVector&,int)const = 0;
};

#endif // EQUATIONPROPERTIES_H
