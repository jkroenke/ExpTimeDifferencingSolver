#include <iostream>
#include <vector>
#include <cmath>

//local imports
#include "equationproperties.h"
#include "ksequationproperties.h"
#include "exptimedifferencing.h"
#include "output.h"
#include "complexvector.h"
#include "dust.h"


int main()
{
    //Problem parameters
    double length=20;
    double timestep=0.01;
    double max_time=100;
    const int max_mode_index=12;

    //Initialization of equation object
    EquationProperties *equation;
    equation=new KSEquationProperties(length);
    equation->setLinearized(false);

    //Initialization of solver object
    ExpTimeDifferencing solver=ExpTimeDifferencing(equation,max_mode_index);

    //Set initial values
    bool fileinit=true;
    if(fileinit)
    {
        ComplexVector coefficients=ComplexVector("../output/saved_state.dat");
        solver.setCoefficients(coefficients);
    }
    else
    {
        ComplexVector coefficients=ComplexVector();
        coefficients.initialize(max_mode_index,0,0);
        coefficients.setCoefficient(0,1,1);
        solver.setCoefficients(coefficients);
    }

    //Iteration
    Output plotter=Output();
    Dust dust=Dust(100,0,length);
    const int plot_max_num=1000;
    int plot_num=1;
    double time=0;
    plotter.save(time,solver.getCurrentState());
    plotter.save(time,dust);
    while(time<max_time)
    {
        solver.iterate(timestep);
        dust.move(solver.getCurrentState(),timestep);
        time+=timestep;
        if(time>(plot_num*max_time/plot_max_num))
        {
            plotter.save(time,solver.getCurrentState());
            plotter.save(time,dust);
            plot_num++;
        }
    }

    //Save Output
    plotter.hardsave("../output/saved_state.dat",solver.getCurrentState());
    plotter.fileprint("../output/ufield.dat",314);
    plotter.fileprint("../output/mode_states.dat");
    plotter.fileprintPos("../output/pos_list.dat");
    return 0;
}
