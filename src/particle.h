#include "complexvector.h"

#ifndef PARTICLE_H
#define PARTICLE_H

class Particle
{
private:
    double position;
public:
    double getLocaleVelocity(ComplexVector&);
    double getPosition()const;
    void move(ComplexVector&,double);
    Particle(double);
};

#endif // PARTICLE_H
